'''
Class that creates Binary object to do math with Binary numbers
(addition, subtraction, equal, negation, less than, integer equivalent, absolute value)
'''

class Binary:
    def __init__(self,bin_str='0'):
        for i in range(len(bin_str)):
            if bin_str[i]!='0' and bin_str[i]!='1' or len(bin_str)>16:
                raise RuntimeError()
        self.num_list = []
        if bin_str=='':
            bin_str='0'
        for i in range(len(bin_str)):
            self.num_list.append(int(bin_str[i]))
        self.pad()
        
    def pad(self):
        for i in range(16-len(self.num_list)):
                self.num_list.insert(0,self.num_list[0])
                
    def __repr__(self):
        bin_num = ''
        for i in range(16):
            bin_num += str(self.num_list[i])
        return bin_num

    def __eq__(self,bin_obj):
        if self.num_list == bin_obj.num_list:
            return True
        else:
            return False   
  
    def __add__(self,other):
        lnum_list = []
        carry = 0
        for i in range(15,-1,-1):
            bitsum = self.num_list[i] + other.num_list[i] + carry
            lnum_list.insert(0,bitsum%2)
            carry = int(bitsum>1)
        if self.num_list[0]==other.num_list[0] and self.num_list[0]!=lnum_list[0]:
            raise RuntimeError('overflow')
        return Binary(''.join([str(i) for i in lnum_list]))
        
    def __neg__(self):
        neg_list = []
        for i in range(16):
            if self.num_list[i]==1:
                neg_list.append(0)
            else:
                neg_list.append(1)
        one = Binary('01')
        new = one + Binary(''.join([str(i) for i in neg_list]))
        return new
    
    def __sub__(self,bin_obj):
        minus = -bin_obj
        sum = self + minus
        return sum
    
    def __int__(self):
        decimal = 0
        if self.num_list[0]==1:
            decimal = (-1)*(2**15)
        for i in range(1,16):
            decimal += (2**(15-i))*self.num_list[i]
        return decimal
        
    def __lt__(self,bin_obj):
        if self.num_list[0]==1 and bin_obj.num_list[0]!=1:
            return True
        if self.num_list[0]==0 and bin_obj.num_list[0]==1:
            return False
        dif = self - bin_obj
        if self.num_list[0]==1 and bin_obj.num_list[0]==1 and dif.num_list[0]==0:
           return False
        if self.num_list[0]==0 and bin_obj.num_list[0]==0 and dif.num_list[0]==0:
           return False
        return True
    
    def __abs__(self):
        x = str(self)
        if self.num_list[0]==1:
            x = str(-self)
        return Binary(x)
#==========================================================
def main():
    ''' 
    function testing.
    '''

if __name__ == '__main__':
    main()
